<?php

namespace Drupal\notifications_widget_users_limit\Services;

use Drupal\notifications_widget\Services\NotificationsWidgetService;

/**
 * If a current user exist in the "field_checker" then display a notification.
 */
class NotificationsWidgetUsersLimitService extends NotificationsWidgetService {

  /**
   * Stores the data into notification table.
   */
  public function logNotification($message, $userAction, $entity) {
     
    if (!$entity->hasField('field_checker')) {
      return;
    }

    $allowed_users = $entity->field_checker->referencedEntities();
    
    foreach($allowed_users as $allowed_user) {

      if ($allowed_user->id() == $this->currentUser->id()) {
        parent::logNotification($message, $userAction, $entity);
        return;        
      }      
    }
  }

}
